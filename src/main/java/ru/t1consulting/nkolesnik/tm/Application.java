package ru.t1consulting.nkolesnik.tm;

import ru.t1consulting.nkolesnik.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
