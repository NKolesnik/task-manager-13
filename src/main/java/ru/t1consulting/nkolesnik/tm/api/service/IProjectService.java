package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    void clear();

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project removeByIndex(Integer index);

    Project removeById(String id);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
